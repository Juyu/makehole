// From Mobile/Diffuse
Shader "HoleGame/Floor" {

    Properties {
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _Color("Color", Color) = (1,1,1,1)

        _StencilComp ("Stencil Comparison", Float) = 0
        _Stencil ("Stencil ID", Float) = 0
        _StencilOp ("Stencil Operation", Float) = 0
        _StencilWriteMask ("Stencil Write Mask", Float) = 255
        _StencilReadMask ("Stencil Read Mask", Float) = 255
        _ColorMask ("Color Mask", Float) = 15
    }

    SubShader {
        Tags { 
            "RenderType"="Opaque" 
        }
        
        Stencil
        {
            Ref [_Stencil]
            Comp [_StencilComp]
            Pass [_StencilOp]
            ReadMask [_StencilReadMask]
            WriteMask [_StencilWriteMask]
        }
        ColorMask [_ColorMask]

        CGPROGRAM

        #pragma surface surf Lambert noforwardadd

        sampler2D _MainTex;
        fixed4 _Color;

        struct Input {
            float2 uv_MainTex;
        };

        void surf (Input IN, inout SurfaceOutput o) {
            fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
            o.Albedo = c.rgb * _Color;
            o.Alpha = c.a * _Color.a;
        }
        
        ENDCG
    }

    Fallback "Mobile/VertexLit"
}