﻿
Shader "HoleGame/Hole" {
    Properties {
        _Color("Color", Color) = (1,1,1,1)

        _StencilComp ("Stencil Comparison", Float) = 0
        _Stencil ("Stencil ID", Float) = 0
        _StencilOp ("Stencil Operation", Float) = 0
        _StencilWriteMask ("Stencil Write Mask", Float) = 255
        _StencilReadMask ("Stencil Read Mask", Float) = 255
        _ColorMask ("Color Mask", Float) = 15
    }

    SubShader {

        Tags { 
            "RenderType"="Opaque" 
        }
        
        ZWrite Off
        // 关闭深度写入，避免挡住内部物体；

        Stencil
        {
            Ref [_Stencil]
            Comp [_StencilComp]
            Pass [_StencilOp]
            ReadMask [_StencilReadMask]
            WriteMask [_StencilWriteMask]
        }
        ColorMask [_ColorMask]

        Pass{

            CGPROGRAM
                fixed4 _Color;

                #include"UnityCG.cginc"

                #pragma vertex vert
                #pragma fragment frag

                v2f_img vert(appdata_full v)
                {
                    v2f_img f;
                    f.pos = UnityObjectToClipPos(v.vertex);
                    return f;
                }

                fixed4 frag(v2f_img i) : SV_Target
                {
                    // 当_ColorMask为15的时候可以洞所在的地方；
                    return _Color;
                }
                
            ENDCG
        }
    }

    Fallback "Mobile/VertexLit"
}