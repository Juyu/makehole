﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hole : MonoBehaviour
{
    [Header("块的Tag")]
    public string[] BlockTags;
    [Header("块本身的Layer")]
    public string BlockOriginLayerName;
    [Header("块成为掉落快的Layer")]
    public string FallingBlockLayerName;

    [Space(32)]
    public LayerModifier HoleZone;
    public LayerModifier[] WallZones;

    void Start()
    {
        HoleZone.TargtGameObjectTags = BlockTags;
        HoleZone.TargtLayerName = FallingBlockLayerName;
        foreach(LayerModifier modifier in WallZones){
            modifier.TargtGameObjectTags = BlockTags;
            modifier.TargtLayerName = BlockOriginLayerName;
        }
    }

}
