﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerModifier : MonoBehaviour
{
    public bool ModifyOnTriggerStay;
    public string[] TargtGameObjectTags;
    public string TargtLayerName;


    void OnTriggerEnter(Collider other)
    {
        if(NeedModifyTargetLayer(other.gameObject)){
            ChangeToTargetLayer(other.gameObject);
        }
    }

    void OnTriggerStay(Collider other)
    {
        if(!ModifyOnTriggerStay) return ;
        if(NeedModifyTargetLayer(other.gameObject)){
            ChangeToTargetLayer(other.gameObject);
        }
    }

    private bool NeedModifyTargetLayer(GameObject targetObject){
        if(TargtGameObjectTags.Length == 0){
            return false;
        }
        foreach(string tag in TargtGameObjectTags){
            if(tag != null && tag.Equals(targetObject.tag)) return true;
        }
        return false;
    }

    private void ChangeToTargetLayer(GameObject targetObject){
        targetObject.layer = LayerMask.NameToLayer(TargtLayerName);
    }
}
