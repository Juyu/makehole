# 原理
视觉的洞，使用了模板测试。

物理上的洞，利用不同Layer之间相互没有力的作用来实现穿透。

# 用法

## 材质

地板要使用材质Assets/MakeHole/ShadersAndMaterial/Floor。

## Layer
需要准备三个Layer：
1. 地板所使用的Layer，称为LayerA；
2. 块本身的Layer，称为LayerB；
3. 块要穿透地板所使用的Layer，称为LayerC；

其中，LayerA与LayerB有物理交互，而LayerA与LayerC没有物理交互。

## Tag
为所有的块准备Tag，可以有多个。

## 预制体Hole

直接拖进场景，并配置Hole。
1. Block Tags：上文提到的Tag；
2. Block Origin Layer Name：上文提到的LayerB；
3. Falling Block Layer Name：上文提到的LayerC；